hook.Add("PlayerSpawnProp", "Spawnmenu_Spawncheck", function(ply, model)
	if (ply:IsAdmin()) then return true end
	for k, v in pairs(Spawnmenu_PropList) do
		for id, _model in pairs(v.Props) do
			if (model == _model && (v.IsAllowed && v:IsAllowed(ply))) then return true end
		end
	end
	
	return false
end)

function GMODTool(ply, command, arguments)
	if ( arguments[1] == nil ) then return end
	local toolRestrict = FindRestrictToolByName(arguments[1])
	if (isfunction(toolRestrict) and not toolRestrict(ply)) or toolRestrict == nil then
			CC_GMOD_Tool(ply, command, arguments)
	end
end

concommand.Remove("gmod_tool")
concommand.Add( "gmod_tool", GMODTool, nil, nil, { FCVAR_SERVER_CAN_EXECUTE } )
