Spawnmenu_PropList = {
	{
		Name = "Construction",
		Icon = "icon16/building.png",
		IsAllowed = function(ply)
			return true
		end,
		Props = {
			"models/baril/baril.mdl"
		}
	},
	{
		Name = "Decoration",
		Icon = "icon16/photo.png",
		Props = {
			"models/buche/buche.mdl"
		}
	},
	{
		Name = "Police",
		Icon = "icon16/shield.png",
		Props = {}
	},
	{
		Name = "Restauration",
		Icon = "icon16/note.png",
		Props = {
			"models/cuivre/cuivre.mdl",
			"models/coup/coup.mdl"
		}
	},
	{
		Name = "Protection",
		Icon = "icon16/user.png",
		Props = {
			"models/chairs/armchair.mdl"
		}
	}
}

Spawnmenu_RestrictToolList = {
	["axis"] = function(ply)
		return true
	end,
	["rb655_easy_inspector"] = function(ply)
		return true -- not ply:IsAdmin()
	end
}
