function FindRestrictToolByName(name)	
	for k, v in pairs(Spawnmenu_RestrictToolList) do
		if (k == name) then return v end
	end
	
	return nil
end

hook.Add("CanTool", "ToolGun_Restrict", function(ply, tr, tool)
	local toolRestrict = FindRestrictToolByName(tool)
	if isfunction(toolRestrict) then
		return not toolRestrict(ply)
	else
		return true
	end
end)
