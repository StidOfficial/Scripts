hook.Add("PopulateProps", "AddPropContent", function(pnlContent, tree, node)
	for k, v in pairs(Spawnmenu_PropList) do
		if (v.IsAllowed && !v:IsAllowed(LocalPlayer())) then continue end
		local node = tree:AddNode(v.Name, v.Icon)
		node.DoPopulate = function(self)
			if (self.PropPanel) then return end
			
			self.PropPanel = vgui.Create("ContentContainer", pnlContent)
			self.PropPanel:SetVisible(false)
			self.PropPanel:SetTriggerSpawnlistChange(false)
			
			for num, model in pairs(v.Props) do
				local cp = spawnmenu.GetContentType("model")
				if (cp) then
					cp(self.PropPanel, {model = model})
				end
			end
		end
		
		node.DoClick = function(self)
			self:DoPopulate()
			pnlContent:SwitchPanel(self.PropPanel)
		end
	end
end)

hook.Add("PopulateToolMenu", "SpawnMenuToolRestrict", function()
	local PANEL = baseclass.Get("ToolMenu")
	function PANEL:LoadTools()
		local tools = spawnmenu.GetTools()
	
		for strName, pTable in pairs( tools ) do
			for kCategory, vCategory in pairs(pTable.Items) do
				for kTool, vTool in pairs(vCategory) do
					if not istable(vTool) then continue end
					local toolRestrict = FindRestrictToolByName(vTool.ItemName)
					if isfunction(toolRestrict) and toolRestrict(LocalPlayer()) then
						pTable.Items[kCategory][kTool] = nil
					end
				end
			end
			
			self:AddToolPanel( strName, pTable )
		end
	end

	baseclass.Set("ToolMenu", PANEL)
end)

hook.Add("AddToolMenuTabs", "SpawnMenuTab", function()
	--[[
	spawnmenu.RemoveCreationTab("#spawnmenu.content_tab")
	spawnmenu.RemoveCreationTab("#spawnmenu.category.weapons")
	spawnmenu.RemoveCreationTab("#spawnmenu.category.npcs")
	spawnmenu.RemoveCreationTab("#spawnmenu.category.entities")
	spawnmenu.RemoveCreationTab("#spawnmenu.category.vehicles")
	spawnmenu.RemoveCreationTab("#spawnmenu.category.postprocess")
	spawnmenu.RemoveCreationTab("#spawnmenu.category.dupes")
	spawnmenu.RemoveCreationTab("#spawnmenu.category.saves")
	]]
	spawnmenu.ClearCreationTabs()
	
	spawnmenu.AddCreationTab("#spawnmenu.content_tab", function()
		local ctrl = vgui.Create("SpawnmenuContentPanel")
		ctrl:CallPopulateHook("PopulateProps")

		return ctrl
	end, "icon16/application_view_tile.png", -10)
end)
